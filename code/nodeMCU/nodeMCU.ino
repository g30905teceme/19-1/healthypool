#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
SoftwareSerial s(D6, D5);

const char* ssid     = "j4";
const char* password = "rosa123456";
const char* host = "192.168.43.154";

//const char* ssid     = "VPR";
//const char* password = "veproser2018";
//const char* host = "192.168.0.100";


float data;
void setup() {
  // Initialize Serial port
  Serial.begin(9600);
  s.begin(9600);
  //while (!Serial) continue;

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {

  Serial.println("########");
  s.write("s");
  if (s.available() > 0)
  {
    data = s.read();
    Serial.println(data, 2);
    delay(3000);
  }

  Serial.print("connecting to ");
  Serial.println(host);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 8888;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;

  }
  
  String url = "/proyecto_arduino/public/ph_list/create.php?";
   url += "value=";
  url += data;

  Serial.print("Requesting URL: ");
  Serial.println(url);

  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from server and print them to Serial
  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }

  Serial.println();
  Serial.println("closing connection");
  delay(10000);
}
